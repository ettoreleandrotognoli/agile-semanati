---
marp: true
theme: uncover
---
<style>
section {
    text-align: left;
    font-size: 28px;
}
ul {
    margin-left: 0px;
}
section.center p, section.center ul, section.center li,
section.center h1,  section.center h2, section.center h3,
section.center h4,  section.center h5, section.center h6  {
    text-align: center;
}
section h1 {
    font-size: 40px;
}
section::after {
  font-weight: bold;
}
section.cover header{
    text-align: right;
    opacity: 0.1;
}
section.title h1 {
  font-size: 55px;
}
section.title footer {
  text-align: right;
}
</style>

<!-- _class: center invert cover title -->

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->

<!-- _footer: Powered by [Marp](https://yhatt.github.io/marp/) & [PlantUML](https://github.com/plantuml) -->
# Desenvolvimento de Software Ágil
<!-- _backgroundImage: url(./link.svg) -->
<!-- _backgroundSize: cover -->

Ettore Leandro Tognoli
ettore.leandro.tognoli@gmail.com

---


![bg fit](./resources/qrcode.svg)


<!-- _footer: https://ettoreleandrotognoli.gitlab.io/agile-semanati/ -->

---

<style scoped>
p {
    font-size: 20px
}
</style>


# Sobre Mim

Ettore Leandro Tognoli
ettore.leandro.tognoli@gmail.com
https://github.com/ettoreleandrotognoli

![bg right fit](./resources/me.jpg)

[![Github Badge](https://img.shields.io/badge/Github--blue?style=social&logo=github&link=https://github.com/ettoreleandrotognoli)](https://github.com/ettoreleandrotognoli)[![Gitlab Badge](https://img.shields.io/badge/Gitlab--blue?style=social&logo=gitlab&link=https://gitlab.com/ettoreleandrotognoli)](https://gitlab.com/ettoreleandrotognoli)[![Linkedin Badge](https://img.shields.io/badge/Linkedin--blue?style=social&logo=linkedin&link=https://www.linkedin.com/in/ettore-leandro-tognoli/)](https://www.linkedin.com/in/ettore-leandro-tognoli/)[![Discord Badge](https://img.shields.io/badge/Discord--blue?style=social&logo=discord&link=https://discord.gg/Mr6yxp4ZE2)](https://discord.gg/Mr6yxp4ZE2)[![Telegram Badge](https://img.shields.io/badge/Telegram--blue?style=social&logo=telegram&link=https://t.me/ettoreleandrotognoli)](https://t.me/ettoreleandrotognoli)[![Instagram Badge](https://img.shields.io/badge/Instagram--blue?style=social&logo=instagram&link=https://www.instagram.com/ettoreleandrotognoli/)](https://www.instagram.com/ettoreleandrotognoli/)[![Twitter Badge](https://img.shields.io/badge/Twitter--blue?style=social&logo=twitter&link=https://twitter.com/EttoreLeandro)](https://twitter.com/EttoreLeandro)

---

<!-- paginate: true -->



<!-- header: Sobre mim -->

## Formação

Bacharelado em Ciência da Computação - UNIVEM ( 2012 )

Mestrado em Ciência da Computação - UFSCar ( 2016 )

## Experiência

Programador/Desenvolvedor em algumas empresas em Marília

Consultor e Programador Freelancer

Professor de Graduação - FATEC Pompeia

Professor de Pós-Graduação - UNIVEM

Engenheiro de Software - Red Hat

---


![bg contain](./resources/ada.jpg)
![bg contain](./resources/git.jpg)
![bg contain](./resources/lua.jpg)
![bg contain](./resources/rust.jpg)
![bg contain](./resources/shell.jpg)

<!-- _footer: Ada :cat:, Git :cat:, Lua :cat:, Rust :dog: e Shell :cat: -->


---

<!-- _class: invert center -->

# Público alvo

Desenvolvedores, programadores, gestores, demais profissionais  relacionados com desenvolvimento de software


---

<!-- header: Público alvo -->

# O que é Software?

Hardware vs Software

Hardware você chuta e software você xinga 😂

"Ware" Produto, mercadoria

"Soft" fácil mudança

---

# Software


"O software tem dois valores, comportamento e estrutura."

Comportamento é o que seu produto realmente faz

A qualidade da estrutura garante a facilidade ( baixos custos ) de mudança


---

O importante é estar funcionando!? 😕

Algo que não está funcionando posso fazer funcionar, já algo que não consigo alterar não vai me atender para sempre.

*"Software funcionando é a medida primária de progresso."* [Manifesto Ágil](https://agilemanifesto.org/iso/ptbr/principles.html)

---

# Para os demais "Metodologia" [XGH](https://gohorseprocess.com.br/extreme-go-horse-xgh/)

1- Pensou, não é XGH.
2- Existem 3 formas de se resolver um problema, a correta, a errada e a XGH, que é igual à errada, só que mais rápida.
...
16- Não tente remar contra a maré.

Caso seus colegas de trabalho usem XGH para programar e você é um coxinha que gosta de fazer as coisas certinhas, esqueça! Pra cada Design Pattern que você usa corretamente, seus colegas gerarão 10 vezes mais código podre usando XGH.

---

<!-- header: "" -->

# Apenas minha opinião

Não estou aqui representando a Red Hat.

Minha opinião baseada na minha experiência e na experiência de alguns grandes nomes.

---

# Bibliografia

Clean Architecture - Robert C Martin
Clean Code - Robert C Martin
Clean Coder - Robert C Martin
Clean Agile - Robert C Martin
Design Patterns - GoF
Domain Drive Design - Eric Evans
SCRUM A Arte de Fazer o Dobro do Trabalho na Metade do Tempo - Jeff
Sutherland & J.J. Sutherland
A Startup Enxuta - Eric Ries

---

<!-- _class: center title invert-->

# Ágil

---

<!-- header: Ágil -->

# O nome Ágil?

Reunião sobre processos leves em Snowbird - Utah , 11-13 Fevereiro 2001

As opções: "Adaptável", "Peso Leve", "Ágil"

Aparentemente Ágil foi escolhido por ser um nome popular nas Forças Armadas da época

Foco em equipes pequenas de software


<!-- _footer: Clean Agile -->

---


# O que é?

"A agilidade é um processo em que um projeto é subdividido em iterações.
A saída de cada iteração é calculada e usada para avaliar continuamente o planejamento.
As funcionalidades são implementadas de acordo com o valor de negócios agregado, de modo que as coias mais valiosas sejam implementadas primeiro.
O nível de qualidade é mantido o mais alto possível.
O cronograma é principalmente gerenciado conforme a manipulação do escopo."

<!-- _footer: Clean Agile -->

---


# O que é?

"O ágil é um framework de disciplinas que sustenta o desenvolvimento profissional de software"

"A metodologia ágil não é um processo, não é modismo e não é somente um conjunto de regras. Pelo contrário, a agilidade é um conjunto de direitos, expectativas e disciplinas do tipo que alicerça a base de uma profissão ética"

<!-- _footer: Clean Agile -->

---

# Pilares


__Pessoa e interações__, em detrimento de processos e ferramentas
__Validação do software__, em vez de uma documentação exaustiva e longa
__Colaboração com o cliente__, em detrimento da negociação de contrato
__Reposta à mudança__, em vez de seguir um plano cegamente


<!-- _footer: Clean Agile -->

---

<!-- _class: center invert  cover title -->

# Um pouco de prática


---

<!-- header: Um pouco de prática -->

# Histórias

"Histórias estão sempre sendo escritas, descartadas e desenvolvidas ao longo do projeto"

__INVEST__

- Independentes ( Independent )
- Negociaveis  ( Negotiable )
- De Valor ( Valuable )
- Estimaveis ( Estimable )
- Pequenas ( Small )
- Testaveis ( Testable )

---

# Exemplos

- Como motorista de um carro, para aumentar a velocidade pressionarei meu pé com mais força no acelerador. ( Acelerar )
- Login: Quando o usuário digitar um nome de usuário e senha válidos e clicar em "login", o sistema apresentará a página "Bem-vindo"
- Ao aproximar um crachá válido no sensor a porta deve abrir

---

# Definindo a Iteração

- Esforço/Custo vs Valor
- Estimativa/Chute Inicial
- Pontos são estimativas e não promessas

![bg right fit](./resources/matriz.svg)

---

# Verificação do Ponto Médio

Na metade da iteração já foi atingido metade dos pontos de história?

Se sim, tudo segue feliz!

Se foi melhor que o esperado, podemos adicionar mais histórias.

Se não, alguns ajustes são necessários.


---


# Gerenciamento do Projeto

Todo projeto de software obedece a regra irrefutável da Restrição Tripla

Bom, rápido, barato e concluído: escolha somente três

"...um bom gerente de projetos entende que esses quatro atributos têm coeficientes. Um gerente competente coordena um projeto para ser bom, rápido e barato o suficiente e concluido quando for necessário."

O ágil ajuda e conta com esse tipo de gerenciamento.


---

# Como ajuda?

Destruindo a esperança o mais cedo possível

__Fornecendo Dados__ importantes para o gerenciamento

A cada iteração os programadores atingem determinados pontos de história.

Sem dados, sem agile


---

![bg contain](./resources/velocity.svg)
![bg contain](./resources/burndown.svg)

---


# Gerenciado a Restrição Tripla

Bom ( Qualidade ), Rápido, Barato, Concluído

- Mudança de Cronograma ( -Rápido )

  Nem sempre é comercialmente viavel

- Adição de Pessoal ( +Rápido, -Barato)

    O ganho de velocidade não é instantaneo
    Aumento de Custo

- Mudanças no Escopo  ( -Concluído )

- ~~Diminuindo a Qualidade~~

---

# Qualidade do Código

Nunca sacrifique a qualidade do código

"A unica forma de avançar rápido é fazendo as coisas bem-feitas."

Extra:

"Retirar um elemento tecnicamente ruim de uma equipe pode ser mais produtivo do que adicionar um elemento melhor capacitado." ( [Net Negative Producing Programmer](http://www.akitaonrails.com/2009/03/30/off-topic-net-negative-producing-programmer) )

---


# Pressionando a Equipe

- Inflação ( Velocidade Crescente )
- Baixa Qualidade ( Velocidade decrescente )
- Ritmo Sustentavel (maratona não sprint)

--- 

# Conclusão

- Negociação do Prazo e do Escopo

- Adição de pessoal, se existir tempo e orçamento o suficiente

- ~~Abrir mão de processos e metodologias~~



---

<!-- header: "" -->

# Declaração de Direitos

"...o objetivo da agilidade era restaurar a divisão entre negócios e desenvolvimento."

- __Direitos do Cliente__

- __Direitos do Desenvolvedor__

---

#

<!-- _header: Direitos do Cliente -->

# Direitos do Cliente

- Você tem direito a um planejamento geral e de saber o que pode ser realizado, quando e a que preço

- Você tem direito de ver o progresso da implementação de um sistema que comprovadamente funcione por meio de testes reproduzíveis determinados por você

- Você tem o direito de mudar de ideia, substituir a funcionalidade e alterar as prioridades sem pagar custos exorbitantes.

- Você tem o direito de ser informado sobre mudanças no cronograma e estimativa a tempo de escolher como reduzir o escopo a fim de cumprir uma data exigida. Você pode cancelar as coisas a qualquer momento e ficar com um sistema funcional útil que reflita o investimento até aquela data.

---

<!-- _header: Direitos do Desenvolvedor -->

# Direitos do Desenvolvedor

- Você tem o direito de saber o que é necessário por meio de declarações de prioridade transparentes.

- Você tem o direito de desenvolver trabalhos da mais alta qualidade o tempo todo.

- Você tem o direito de solicitar e receber ajuda de colegas, gerentes e clientes.

- Você tem o direito de efetuar e atualizar sua próprias estimativas.

- Você tem o direito de aceitar suas responsabilidades em vez de ter alguém que lhes atribua.

---

<!-- _class: title invert center -->

# E o que acontece na prática? ( as vezes )

---

A todo momento chegam novos requisitos, estamos falando de agile, então sempre são bem vindos.
Espera-se que os novos requisitos sejam entregues rapidamente.
O gerente determina o que é rápido junto com a estimativa e o prazo.
O desenvolvedor se sente forçado a fazer o mais rapido possivel e acha que abrindo mão das boas práticas isso será possível.

Sem as disciplinas corretas o agile é a fórmula da bagunça

---


![bg contain](./resources/xp-practices.webp)

---


<!-- _class: title invert center -->

# Desenvolvedor Ágil


---

<!-- header: Desenvolvedor Ágil -->

# Versionamento do Código

Até parece desnecessário falar disso.

git :heart:

---

# Gerenciamento de Dependências

Gradle, Maven, Composer, NPM, Yarn, Pipenv, Cargo

- Automatização da build
- Facilidade para novos membros
- Reúso

---

# Testes Automatizados

TDD, JUnit, Selenium

- Testes unitários
- Testes de aceitação
- Automatização da build

---

# CI/CD
Continuous Integration e Continuous Delivery

Quanto tempo você gasta com deploy?

Gitlab Pipelines
Github Actions
Jenkins
Travis CI
Containers

---

# Código Limpo

Você passa mais tempo lendo código do que escrevendo código, legibilidade importa.

Pense bem nos nomes das funções, métodos, classes, variaveis e etc.

Se você sentiu necessidade de comentar seu código é porque você falhou na expressividade do seu código.

Tenha carinho com seus testes.


<!-- _footer: Clean Code -->

---



# Arquitetura Limpa

"Se uma mudança nos requisitos quebrar sua arquitetura, sua arquitetura não presta."

Não leve para o lado ruim, veja como uma oportunidade para melhora-lá.

Entender como realmente o negócio funciona pode levar tempo.

A qualidade da arquitetura evolui em saltos. ( DDD )

O software tem dois valores, comportamento e estrutura.

---

# SOLID

- Single Responsability Principle
- Open Closed Principle
- Liskov Substitution Principle
- Interface Segregation Principle
- Dependency Inversion Principle

---



# Padrões de Projeto

Soluções versáteis para problemas genéricos

Linguagem Comum ( DDD ), Metáforas ( Agile )


---

# Craftsmanship


Não apenas software em funcionamento, mas __software de excelente qualidade__

Não apenas responder a mudanças, mas __agregar valor de forma constante e crescente__

Não apenas indivíduos e suas interações, mas __uma comunidade de profissionais__

Não apenas a colaboração do cliente, mas __parcerias produtivas__

<!-- _footer: https://manifesto.softwarecraftsmanship.org/#/pt-br -->


---

<!-- header: "" -->
<!-- _class: center invert cover -->

# FIM

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->
<!-- paginate: false -->
<!-- _class: center invert cover -->
<!-- _backgroundImage: url(./link.svg) -->
<!-- _backgroundSize: contain -->

Ettore Leandro Tognoli
ettore.leandro.tognoli@gmail.com
https://github.com/ettoreleandrotognoli

[![Github Badge](https://img.shields.io/badge/Github--blue?style=social&logo=github&link=https://github.com/ettoreleandrotognoli)](https://github.com/ettoreleandrotognoli)[![Gitlab Badge](https://img.shields.io/badge/Gitlab--blue?style=social&logo=gitlab&link=https://gitlab.com/ettoreleandrotognoli)](https://gitlab.com/ettoreleandrotognoli)[![Linkedin Badge](https://img.shields.io/badge/Linkedin--blue?style=social&logo=linkedin&link=https://www.linkedin.com/in/ettore-leandro-tognoli/)](https://www.linkedin.com/in/ettore-leandro-tognoli/)[![Discord Badge](https://img.shields.io/badge/Discord--blue?style=social&logo=discord&link=https://discord.gg/Mr6yxp4ZE2)](https://discord.gg/Mr6yxp4ZE2)[![Telegram Badge](https://img.shields.io/badge/Telegram--blue?style=social&logo=telegram&link=https://t.me/ettoreleandrotognoli)](https://t.me/ettoreleandrotognoli)[![Instagram Badge](https://img.shields.io/badge/Instagram--blue?style=social&logo=instagram&link=https://www.instagram.com/ettoreleandrotognoli/)](https://www.instagram.com/ettoreleandrotognoli/)[![Twitter Badge](https://img.shields.io/badge/Twitter--blue?style=social&logo=twitter&link=https://twitter.com/EttoreLeandro)](https://twitter.com/EttoreLeandro)

Obrigado!
