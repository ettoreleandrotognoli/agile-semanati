import random
import pygal

size = 10
velocity = 20

initial_points = velocity * size

velocity_data = [ int(random.uniform(velocity - 2, velocity + 2)) for _ in range(size)]

increase_points = [int(random.uniform(velocity / 2, velocity * 2)) if random.random() > 0.8 else 0 for _ in range(size)]

burndown_data = [ initial_points - sum(velocity_data[:i]) + sum(increase_points[:i]) for i in range(size) ]

print(velocity_data)
print(burndown_data)

velocity_chart = pygal.Bar(title='Velocidade', show_legend=False)
velocity_chart.add('Pontos', velocity_data)
velocity_chart.x_labels = [ f'It {i+1}' for i in range(size)]
velocity_chart.y_title = 'Pontos'
velocity_chart.x_title = 'Iteração'
velocity_chart.render_to_file('velocity.svg')

burndown_chart = pygal.Bar(title='Burndown', show_legend=False)
burndown_chart.add('Pontos', burndown_data)
burndown_chart.x_labels = [ f'It {i+1}' for i in range(size)]
burndown_chart.y_title = 'Pontos Restantes'
burndown_chart.x_title = 'Iteração'
burndown_chart.render_to_file('burndown.svg')